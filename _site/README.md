## Community Matrix chatroom rules

You will find here the default rules for the rooms part of the [PSA Community](https://matrix.to/#/#psa:anonymousplanet.net). Please migrate from the old Matrix space to the new one.

**Note:** Rooms which are part of the community keep their sovereignty and can apply their own local rules, each with their own local moderation. Some of the rooms are following the general rules, but some are not.

- [Admins](#admins)
- [General Rules](#general)
- [Nothing To Hide Privacy Room Rules](#nth)
- [Modern Cryptography Room Rules](#moderncrypto)
- [OS Security Room Rules](#ossecurity)
- [Bnonymity Room Rules](#bnonymity)
- [Exceptions](#exceptions)
- [Bans](#bans)
- [Registration](#register)
- [Ban Appeals](#appeals)

### Admins<a name="admins"></a>
- Babba27: [@babba27:anonymousplanet.net](https://matrix.to/#/@babba27:anonymousplanet.net)
- No: [@nope:anonymousplanet.net](https://matrix.to/#/@nope:anonymousplanet.net) ([PGP](https://keyoxide.org/hkp/nopenothinghere%40proton.me))

### General default rules for PSA rooms **unless specific rules are mentioned below**<a name="general"></a>
These are currently only enforced on **#Anonymity**, **#Security**, **#OSINT**, and **#psa-ot** and are NOT applied in rooms with their own rules below.

- Keep it legal
- English only
- Be respectful to each other
- Avoid FUD and/or disinformation
- Avoid gatekeeping and try to remain welcoming to new users
- No hate speech (no racism, no homophobia, no transphobia, etc.)
- No spammerino
- No doxxing unless you have express permission and you move to ```#bnonymity```
- No trolling (this doesn't mean sarcasm is forbidden)
- No NSFW content (no porn, no gore, no Hentai, etc.)
- No upload of any non-media files (no binaries, executables, compressed files, etc.)
- No voice messages (if applicable)
- **Avoid drifting too much off-topic or move to an off-topic room like #bnonymity**

Some exceptions can apply, see the [exceptions](#exceptions) section at the bottom of this page. Violations will be handled at the discretion of the acting moderator.

### Rules for Nothing To Hide Privacy<a name="nth"></a>
- Zero tolerance for discussion of how to commit illicit acts
- Limit political discussion to privacy-related topics only
- No suspicious links or uploading of any non-media files
- Be respectful

#### Mods
- Lucas: [@rawr:beeper.com](https://matrix.to/#/@rawr:beeper.com)
- Prox: [@the_proxster:midov.pl](https://matrix.to/#/@the_proxster:midov.pl) (basically God)

### Rules for Modern Cryptography<a name="moderncrypto"></a>
See [rules here](moderncrypto-rules.html).

### Rules for OS Security<a name="ossecurity"></a>
See [rules here](https://artemislena.eu/coc.html).

### Rules for Bnonymity<a name="bnonymity"></a>
- Keep it legal (seriously)
- English only (no Russian, no Chinese, English only)
- Be "somewhat" respectful to each other
- No hate speech (No racism, no homophobia, no transphobia, etc.)
- No spammerino (scams, ads, flooding, etc.)
- No NSFW content (no Porn, no Gore, no Hentai, etc.)
- All of the above can result in an insta-ban depending on the severity

#### Mods
- Fractal: [@fractal:matrix.org](https://matrix.to/#/@fractal:matrix.org)

### Exceptions<a name="exceptions"></a>

#### Exceptions for #Anonymity, #Security, and #Bnonymity rooms
- Talks about Sci-Hub and/or LibGen are allowed
- Talks about torrenting anonymously are allowed unless the purpose is blatantly illegal

### Bans<a name="bans"></a>
Currently, the following rooms are sharing a common PSA ban list for serious offenders:
- [#Anonymity](https://matrix.to/#/#anonymity:anonymousplanet.net)
- [#Translations (i18n)](https://matrix.to/#/#translations:anonymousplanet.net)
- [#Security](https://matrix.to/#/#security:anonymousplanet.net)
- [#Bnonymity](https://matrix.to/#/#bnonymity:anonymousplanet.net)
- [#ModernCrypto](https://matrix.to/#/#moderncrypto:anonymousplanet.net)
- [#OSINT](https://matrix.to/#/#osint:anonymousplanet.net)
- [Nothing To Hide Privacy Chat](https://matrix.to/#/#nth-main:anonymousplanet.net)

This means that those PSA bans are effectively applied in all of those rooms and can be issued by mods. See the next section for information about appeals.

### Registration<a name="register"></a>

Effective operation of any network requires the administrators and operators to perform at their peak. To do this, we must enure we can provide the best services possible to those in the network. Registration has been enabled. You can register on the homeserver using this temporary token: `0QW5P~L_II1uTE72`

### Ban Appeals<a name="appeals"></a>
Please DM the PSA mods or admins of the room in question to state your case for appeal.
